FROM alpine:3.7

RUN apk add --no-cache python3 && \
	pip3 install --upgrade pip setuptools && \
	rm -r /root/.cache && \
	ln -s python3 /usr/bin/python

RUN apk --no-cache add docker

ADD . /dockerd
RUN cd /dockerd && python setup.py install

VOLUME /var/lib/docker
